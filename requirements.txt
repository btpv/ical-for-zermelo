flask==3.1.0
zermelo-for-python==2024.11.13.12.7.47
cryptography==44.0.2
icalendar==6.1.1
waitress==3.0.2
