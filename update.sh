START_DIR=$(pwd)/
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )/
cd $SCRIPT_DIR
git pull
docker compose up -d --build
sleep 0.5
docker compose logs 
cd $START_DIR