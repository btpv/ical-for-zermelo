FROM python:3.7
WORKDIR /code
ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0
RUN pip install --upgrade pip
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt --no-cache
EXPOSE 5000
COPY app.py /code/
CMD ["waitress-serve", "--port=5000","--call","app:start"]