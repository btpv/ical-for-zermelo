import json
from zermelo_for_python import zermelo,scheduleitem,schedule
from datetime import datetime
from cryptography.fernet import Fernet
from icalendar import Calendar, Event, vText
from flask import Flask, Response, abort, request
from multiprocessing import Pool
defaultttl = 300
defaultmaxuses = 3
key = b'Y03yhZm7SriQkcgzwDpmpxiK3M9fIHn8rREeNt1FCk0='
fernet = Fernet(key)
mainhtml = """
<style>
    div {"border-style: hidden;"}
</style>
<form method=POST>
    <div>
    <input type="text" name="school" id="school">
    <label for="school">school</label>
    </div>
    <br>
    <div>
    <input type="text" name="username" id="username">
    <label for="username">Username</label>
    </div>
    <br>
    <div>
    <input type="password" name="password" id="password">
    <label for="password">password</label>
    </div>
    <br>
    <input type="submit">
</form>

"""
app = Flask(__name__)
def getschedule(args):
    login,year,week = args
    zml = zermelo(*login)
    # print(zml.get_raw_scedule())
    return zml.get_schedule(year=year,week=week)


@app.route("/", methods=["GET", "POST"])
def main():
    if request.method == "GET":
        return Response(mainhtml, mimetype='text/html')
    elif request.method == "POST":
        store = f"{request.form.get('school')}\n\r\t{request.form.get('username')}\n\r\t{request.form.get('password')}"
        url = f"{request.host_url}schedule.ics?userkey={fernet.encrypt(store.encode()).decode()}"
        return Response(f"<a href={url}>{url}</a>")
    else:
        abort(405)
cache = {}
@app.route("/schedule.ics", methods=["GET"])
def genical():
    school, username, password = request.args.get(
        "school"), request.args.get("username"), request.args.get("password")
    userkey = request.args.get("userkey",f"{username}{school}")
    ttl,maxuses = request.args.get("ttl",defaultttl,int),request.args.get("maxuses",defaultmaxuses,int)
    ignore = json.loads(request.args.get("ignore","[]"))
    if school == None or username == None or password == None:
        if userkey == None:
            abort(401)
        school, username, password = fernet.decrypt(
            userkey.encode()).decode().split("\n\r\t")
    if not (userkey in cache and (datetime.now()-cache.get(userkey)["birth"]).seconds < ttl and cache[userkey]["timesused"] < maxuses ):
        zml = zermelo(school, username, password,debug=True)
        with Pool() as pl:
            schedulelist:list[schedule] = pl.map(getschedule,[[(school, username, password),zml.get_date()[0],zml.get_date()[1]+i] for i in range(-1,10)])
            
        lastmodified = datetime.now()
        cache[userkey] = {
            "schedule": schedulelist,
            "birth": lastmodified,
            "timesused": 1
        }
    else:
        cache[userkey]["timesused"] += 1
        schedulelist = cache[userkey]["schedule"]       
        lastmodified = cache[userkey]["birth"]
    lessons:list[scheduleitem] = []
    for week in schedulelist:
        for lesson in week.appointments:
            
            if any(all(not k in lesson.json or lesson.json.get(k) == v for k,v in item.items()) for item in ignore):
                pass
            elif lesson.appointmenttype == "conflict":
                for i in lesson.actions:
                    if not i.cancelled:
                        lessons.append(i)
            elif lesson.appointmenttype == "choice":
                lesson.subjects = ["do uur"]
                lessons.append(lesson)
            elif lesson.id != None:
                lessons.append(lesson)

    # init the calendar
    cal = Calendar()
    # Some properties are required to be compliant
    name = f'{username}@{school}.zportal.nl'
    cal.add('prodid', name)
    cal.add('version', f'{datetime.now().strftime("%Y.%m.%d.%H.%M.%S")}')
    for lesson in lessons:
        if lesson.cancelled:
            continue
        event = Event()
        event.add('summary', ",".join(lesson.subjects))
        description = f"{lesson.changedescription}\n{','.join(lesson.teachers)}\n{','.join(lesson.groups)}"
        description += f"\nlast update {lastmodified.strftime('%Y-%m-%d %H:%M:%S')}"
        event.add('description', description,encode=False)
        event.add('dtstart', lesson.start)
        event.add('dtend', lesson.end)
        event.add('LAST-MODIFIED', lastmodified)
        event['location'] = vText(",".join(lesson.locations))

        event['uid'] = f"{lesson.id}|{datetime.now().timestamp()}"
        event.add('priority', 5)

        # Add the event to the calendar
        cal.add_component(event)
    # response = make_response(cal.to_ical())
    # response.headers["Content-Disposition"] = "attachment; filename=calendar.ics"
    ical = str(cal.to_ical())[2:-1].strip()
    return Response("%s" % ical.replace("\\r\\n", "\n").replace("\\\\","\\"), mimetype='text/calendar')


print(__name__)
if __name__ == "__main__":
    app.run("0.0.0.0", 6969, False)
else:
    def start() -> Flask:
        return app